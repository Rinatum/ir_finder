import random

from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse

import requests
import json


app = FastAPI()

app.mount("/static", StaticFiles(directory="/tmp/documents"), name="static")
app.mount("/root", StaticFiles(directory="./"), name="root")


@app.get("/")
async def search():
    html_content = '''<form action="/search" method="get">
                  <label for="query">Your query goes here:</label>
                  <input type="text" id="query" name="query"><br><br>
                
                  <input type="submit" value="Submit">
                </form>'''
    return HTMLResponse(content=html_content, status_code=200)


@app.get("/search")
async def search(query: str):
    print(query)
    doc_ids = requests.get(f'http://index:7000/search', params={'query': query}).json()
    print('Resulted docs: ', doc_ids['docs'])
    doc_ids = doc_ids['docs']
    html_content = '''
            <form action="/search" method="get">
            <label for="query">Your new query goes here:</label>
            <input type="text" id="query" name="query"><br><br>
            <input type="submit" value="Submit">
            </form>
    <h1> Results </h1>
    '''
    d = json.load(open('/tmp/mappings/idx_to_filename.json', 'r'))
    for i in doc_ids:
        if str(i) in d:
            filename_by_id = d[str(i)]
            html_content += f'<a href="http://localhost:8000/static/{filename_by_id}">File {i} ' \
                            f'(filename: {filename_by_id.replace("_", "/")})</a><br>'
    return HTMLResponse(content=html_content, status_code=200)
