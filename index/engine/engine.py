import pymongo

from engine.utils import Preprocessor, PreprocessPipeline
from engine.utils import SoundexIndex
from engine.tries import PermutermIndex




class Engine:

    def __init__(self, preprocess_pipeline=PreprocessPipeline()):
        self.preprocessor = Preprocessor(preprocess_pipeline)
        self.mango = pymongo.MongoClient('mango', 27017)
        self.aux = {}
        self.soundex = SoundexIndex()
        self.permuterm = PermutermIndex()

        # Just for test
        db = self.mango["inverted_index"]

        posting = db["one"]
        posting.remove({})

        doc_list = [
            {"doc_id": 1, "doc_path": "Apple st 652"},
            {"doc_id": 2, "doc_path": "Apple st 652"},
            {"doc_id": 3, "doc_path": "Apple st 652"},
            {"doc_id": 4, "doc_path": "Apple st 652"},
        ]
        posting.insert_many(doc_list)

        posting = db["second"]
        posting.remove({})

        doc_list = [
            {"doc_id": 3, "doc_path": "Apple st 652"},
            {"doc_id": 4, "doc_path": "Apple st 652"},
            {"doc_id": 5, "doc_path": "Apple st 652"},
            {"doc_id": 6, "doc_path": "Apple st 652"},
        ]
        posting.insert_many(doc_list)

    def __call__(self, query):
        return self._handle(query)

    def _handle(self, query):
        print(f'ENGINE handles {query}')
        tokens = self.preprocessor(query)
        print(f'TOKENS {tokens}')
        doc_ids = []
        for token in tokens:
            doc_ids.append(set(self._search(token)))

        return set.intersection(*doc_ids)

    def _handle_wildcard(self, token):
        pass

    def _search(self, token):
        print(f'_SEARCH handles {token}')
        index = self.mango['inverted_index']
        postings = index[token]
        print(f'_SEARCH result {postings.find()}')
        return [doc["doc_id"] for doc in postings.find()]

    def update(self, aux):
        # self.aux = aux
        # db = self.mango["inverted_index"]
        # for word, postings in aux.items():
        pass


    # posting = db["word1"]
    # posting.remove({})
    # doc_list = [
    #     {"doc_id": 1, "doc_path": "Apple st 652"},
    #     {"doc_id": 2, "doc_path": "Apple st 652"},
    #     {"doc_id": 3, "doc_path": "Apple st 652"},
    #     {"doc_id": 4, "doc_path": "Apple st 652"},
    # ]
    # x = posting.insert_many(doc_list)
    # # print list of the _id values of the inserted documents:
    # print([doc["doc_id"] for doc in posting.find()])
    #
    # posting = db["word2"]
    # posting.remove({})
    # doc_list = [
    #     {"doc_id": 1, "doc_path": "Apple st 652"},
    #     {"doc_id": 2, "doc_path": "Apple st 652"},
    #     {"doc_id": 3, "doc_path": "Apple st 652"},
    #     {"doc_id": 4, "doc_path": "Apple st 652"},
    # ]
    #
    # x = posting.insert_many(doc_list)
    # # print list of the _id values of the inserted documents:
    # print([doc["doc_id"] for doc in posting.find()])
