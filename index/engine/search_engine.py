from abc import ABC, abstractmethod
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import WordPunctTokenizer
import re
from collections import defaultdict
import jellyfish
from bs4 import BeautifulSoup
import requests
from dataclasses import dataclass
from time import time
import numpy as np
nltk.download('stopwords')
nltk.download('wordnet')


class Base_index(ABC):
    
    def __init__(self):
        super().__init__()
        
    @abstractmethod
    def normalize(text: str) -> str:
        pass
    
    @abstractmethod
    def tokenize(text: str) -> str:
        pass
    
    @abstractmethod
    def lemmatize(text: str) -> str:
        pass
    
    @abstractmethod
    def remove_stop_word(text: str) -> str:
        pass
    
    @abstractmethod
    def make_index(collection: list) -> dict:
        """
        return: {token1: [doc_1, doc_2, ..., doc_n]}
        """
        pass
    
class Inverted_index(Base_index):
    
    def __init__(self):
        self.index = defaultdict(set)
        
    
    @staticmethod
    def normalize(text):
        ret = [i for i in re.split('\W', text.lower()) if i.isalpha()]
        ret = ' '.join(ret)
        return ret
    
    @staticmethod
    def tokenize(text):
        tokenizer = WordPunctTokenizer()
        return tokenizer.tokenize(text)
    
    @staticmethod
    def lemmatize(tokens):
        lemmatizer = WordNetLemmatizer()
        return [lemmatizer.lemmatize(token) for token in tokens]
    
    @staticmethod
    def remove_stop_word(tokens):
        stopWords = set(stopwords.words('english'))
        return [i for i in tokens if i not in stopWords]
    
    @classmethod
    def preprocess(cls, text):
        ret = cls.normalize(text)
        ret = cls.tokenize(ret)
        ret = cls.lemmatize(ret)
        ret = cls.remove_stop_word(ret)
        return ret
    
    def update(self, aux: dict):
        for key, value in aux.items():
            self.index[key] = set.union(self.index[key], value)
        
    def make_index(self, collection):
        index = defaultdict(set)
        for doc_id, doc in enumerate(collection):
            for word in self.preprocess(doc.text):
                documents = index[word]
                documents.add(doc_id)
        self.index = index
        return index
    
    def search(self, query):
        query = self.preprocess(query)
        docs_for_each_token = [self.index[i] for i in query]
        relevant_documents = set.intersection(*docs_for_each_token)
        return relevant_documents
    

class Soundex_inverted_index(Base_index):
    
    def __init__(self, n_morphems = 4):
        self.index = defaultdict(set)

    @staticmethod
    def normalize(text):
        ret = [i for i in re.split('\W', text.lower()) if i.isalpha()]
        ret = ' '.join(ret)
        return ret
    
    @staticmethod
    def tokenize(text):
        tokenizer = WordPunctTokenizer()
        return tokenizer.tokenize(text)
    
    @staticmethod
    def lemmatize(tokens):
        lemmatizer = WordNetLemmatizer()
        return [lemmatizer.lemmatize(token) for token in tokens]
    
    @staticmethod
    def remove_stop_word(tokens):
        stopWords = set(stopwords.words('english'))
        return [i for i in tokens if i not in stopWords]
    
    def preprocess(self, text):
        ret = self.normalize(text)
        ret = self.tokenize(ret)
        ret = self.lemmatize(ret)
        ret = self.remove_stop_word(ret)
        ret = [jellyfish.soundex(token) for token in ret]
        return ret
    
    def update(self, aux: dict):
        for key, value in aux.items():
            skey = jellyfish.soundex(key)
            self.index[skey] = set.union(self.index[skey], value)
            
    def make_index(self, collection):
        index = defaultdict(set)
        for doc_id, doc in enumerate(collection):
            for word in self.preprocess(doc.text):
                index[word].add(doc_id)
        self.index = index
        return index
    
    def search(self, query):
        query = self.preprocess(query)
        relevant_documents = []
        index_keys = np.array(list(self.index.keys()))

        for token in query:
            idx = np.array(list(map(lambda x: jellyfish.levenshtein_distance(token, x), index_keys)))
            close_tokens = index_keys[idx <= 1]
            docs_for_each_close_token = [self.index[i] for i in close_tokens]
            possible_docs = set() if not docs_for_each_close_token else set.union(*docs_for_each_close_token)
            relevant_documents.append(possible_docs)
        relevant_documents =  set() if not relevant_documents else set.intersection(*relevant_documents)
        return relevant_documents
    
    
class Wildcard_index(Base_index):
    
    def __init__(self):
        self.index = defaultdict(set)
        self.trie = TrieIndex()
    
    @staticmethod
    def normalize(text):
        ret = text.lower()
        ret = re.sub('[^a-zA-Z \n*]', ' ', ret)
        return ret
    
    @staticmethod
    def tokenize(text):
        return text.split(' ')
    
    @staticmethod
    def lemmatize(tokens):
        lemmatizer = WordNetLemmatizer()
        return [lemmatizer.lemmatize(token) for token in tokens]
    
    @staticmethod
    def remove_stop_word(tokens):
        stopWords = set(stopwords.words('english'))
        return [i for i in tokens if i not in stopWords and i != '']
    
    @classmethod
    def preprocess(cls, text):
        ret = cls.normalize(text)
        ret = cls.tokenize(ret)
        ret = cls.lemmatize(ret)
        ret = cls.remove_stop_word(ret)
        return ret
    
    def update(self, aux: dict):
        for key, value in aux.items():
            self.index[key] = set.union(self.index[key], value)
        for key in aux:
            self.trie[key] = set.union(self.index[key], value)
            
    def make_index(self, collection):
        index = defaultdict(set)
        self.trie = TrieIndex()
        for doc_id, doc in enumerate(collection):
            for word in self.preprocess(doc.text):
                index[word].add(doc_id)
        self.index = index
        for i in index:
            self.trie[i] = index[i]
        return index
    
    def search(self, query):
        query = self.preprocess(query)
        relevant_documents = []

        for token in query:
            if '*' in token:
                close_tokens = self.trie.search_by_wild_card(token)
                docs_for_each_close_token = [self.index[i] for i in close_tokens]
                possible_docs = set() if not docs_for_each_close_token else set.union(*docs_for_each_close_token)
                relevant_documents.append(possible_docs)
            else:
                relevant_documents.append(self.index[token])
            
        relevant_documents = set() if not relevant_documents else set.intersection(*relevant_documents)
        return relevant_documents
    

class Search_engine():
    
    def __init__(self):
        self.wild_index = Wildcard_index()
        self.index = Inverted_index()
        self.sindex = Soundex_inverted_index()
    
    def upd(self, aux: dict):
        self.wild_index.update(aux)
        self.index.update(aux)
        self.sindex.update(aux)
        
    def search(self, query):
        r1 = self.wild_index.search(query)
        r2 = self.index.search(query)
        r3 = self.sindex.search(query)
        return list(set.union(r1,r2,r3))