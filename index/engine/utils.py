from collections import defaultdict
import itertools
import re

from nltk.tokenize import WordPunctTokenizer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
import nltk
import jellyfish

nltk.download('wordnet')
nltk.download('stopwords')


class PreprocessPipeline:

    def __init__(self):
        self.tokenizer = WordPunctTokenizer()
        self.lemmatizer = WordNetLemmatizer()

    def __call__(self, text):
        result = text
        for preprocessing_func in [self.normalize, self.tokenize, self.lemmatize, self.remove_stop_word]:
            result = preprocessing_func(result)
        return result

    def normalize(self, text):
        ret = [i for i in re.split('\W', text.lower()) if i.isalpha()]
        ret = ' '.join(ret)
        return ret

    def tokenize(self, text):
        return self.tokenizer.tokenize(text)

    def lemmatize(self, tokens):
        return [self.lemmatizer.lemmatize(token) for token in tokens]

    def remove_stop_word(self, tokens):
        stopWords = set(stopwords.words('english'))
        return [i for i in tokens if i not in stopWords]


class Preprocessor:
    def __init__(self, preprocess_pipeline=PreprocessPipeline()):
        self.pipeline = preprocess_pipeline

    def __call__(self, text):
        return self.pipeline(text)


class SoundexIndex:
    def __init__(self):
        self.alphabet = {
            'a': '0',
            'b': '1',
            'c': '2',
            'd': '3',
            'e': '0',
            'f': '1',
            'g': '2',
            'h': '0',
            'i': '0',
            'j': '2',
            'k': '2',
            'l': '4',
            'm': '5',
            'n': '5',
            'o': '0',
            'p': '1',
            'q': '2',
            'r': '6',
            's': '2',
            't': '3',
            'u': '0',
            'v': '1',
            'w': '0',
            'x': '2',
            'y': '0',
            'z': '2'
        }
        self.mapping = defaultdict(set)

    def add(self, collection):
        for word in collection:
            word_soundex = self.get_soundex_code(word)
            self.mapping[word_soundex].add(word)

    def get_soundex_code(self, word):
        try:
            soundex = word[0]
            fully_mapped = ''.join([self.alphabet[symbol] for symbol in word[1:].lower()])
            without_cons_and_zeros = ''.join(symbol for symbol, _ in itertools.groupby(fully_mapped)).replace('0', '')
            code_len = len(without_cons_and_zeros)
            code = without_cons_and_zeros[:3] if code_len >= 3 else without_cons_and_zeros + '0' * (3 - code_len)
            return soundex + code
        except KeyError as e:
            raise KeyError(f'{word} contains symbol which is not in English alphabet.')

    def get_mapping(self):
        return self.mapping

    def get_nearest(self, word):
        word_soundex = self.get_soundex_code(word)
        nearest_words = self.mapping.get(word_soundex)
        if nearest_words:
            nearest = list(nearest_words)
            distances = [jellyfish.levenshtein_distance(word, near) for near in nearest]
            min_dist = min(distances)
            return [near for i, near in enumerate(nearest) if distances[i] == min_dist]
        else:
            return []
