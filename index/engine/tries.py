from typing import Any


class Trie:
    def __init__(self, value='', parent=None):
        self.__subtrees = {}
        self.__parent = parent
        self.__value = value

    def __setitem__(self, key: str, value: Any):
        head, tail = key[0], key[1:]
        subtree = self.__subtrees.get(head)

        if not subtree:
            subtree = Trie(parent=self)
            self.__subtrees[head] = subtree

        if tail:
            subtree[tail] = value
        else:
            subtree.__value = value

    def get(self, key, default=None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default

    def __getitem__(self, key):
        value = self.__get(key)
        if value not in ['', None]:
            return value
        else:
            raise KeyError(key)

    def __get(self, key):
        head, tail = key[0], key[1:]

        if head in self.__subtrees:
            subtree = self.__subtrees[head]
        else:
            return None

        if tail:
            value = subtree.__get(tail)
            return value

        elif subtree.__value not in ['', None]:
            return subtree.__value

        else:
            return None

    def keys(self):
        vocabular = []
        self.__keys__('', vocabular)
        return vocabular

    def __keys__(self, current_sequence, vocabular):
        if self.__value != '':
            vocabular.append(current_sequence)

        for subroot, subtree in self.__subtrees.items():
            subtree.__keys__(current_sequence + subroot, vocabular)

    def get_subtree_by_prefix(self, prefix):
        head, tail = prefix[0], prefix[1:]

        if not tail:
            return self.__subtrees.get(head)

        if head in self.__subtrees:
            return self.__subtrees.get(head).get_subtree_by_prefix(tail)
        else:
            return None


class PermutermIndex(Trie):
    def __setitem__(self, key, value):
        subkeys = self.__get_all_shifts(key + '$')
        for subkey in subkeys:
            super(PermutermIndex, self).__setitem__(subkey, value)

    @staticmethod
    def __get_all_shifts(word):
        return [word[i::] + word[:i:] for i in range(len(word))]

    def __getitem__(self, key):
        try:
            value = super(PermutermIndex, self).__getitem__(key + '$')
        except KeyError:
            raise KeyError(key)
        else:
            return value

    def get(self, key, default=None):
        return super(PermutermIndex, self).get(key, default)

    def keys(self):
        return [key[1:] for key in super(PermutermIndex, self).keys() if '$' == key[0]]

    def search_by_wild_card(self, query):
        assert '*' in query, 'Wildcard query should contain *'
        prefix = self.to_prefix_form(query + '$')
        subtree = self.__search_by_prefix(prefix[:-1])
        return subtree

    def __search_by_prefix(self, prefix):
        subtree = self.get_subtree_by_prefix(prefix)
        if subtree:
            return set([''.join((prefix + key).split('$')[::-1]) for key in subtree.keys()])
        else:
            return set()

    @staticmethod
    def to_prefix_form(word):
        shift = word.index('*') - (len(word) - 1)
        return word[shift::] + word[:shift:]
