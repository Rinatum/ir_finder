from subprocess import call
import os
import time

from fastapi import FastAPI, Request

from engine.engine import Engine


def build_app():
    engine = Engine()

    app = FastAPI()

    @app.get("/health-check")
    def health_check():
        return {
            "ok": True
        }

    @app.get("/search")
    def search(query: str):
        print('I AM INDEX')
        print(f'QUERY IS {query}')
        result = engine(query)
        return {'docs': result}

    @app.post("/update")
    def update(aux: dict):
        pass

    return app


time.sleep(10)
app = build_app()

# if __name__ == "__main__":
#     app =
#     call(f"uvicorn api:app --host 0.0.0.0 --port {PORT}", shell=True)
