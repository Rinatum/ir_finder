from utils import Crawler as Downloader
from inverted_index import Inverted_index
import os
from collections import defaultdict
import time, schedule
import json
import requests
import colorlog
import logging

LOGGING_LEVEL = 'INFO'
LOGGING_FORMAT = '[%(asctime)s] %(log_color)s%(message)s'


def get_logger(name=None):
    logger = colorlog.getLogger(name)
    logger.setLevel(logging.getLevelName(LOGGING_LEVEL))

    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter(LOGGING_FORMAT, datefmt='%Y-%m-%d %H:%M:%S'))

    logger.addHandler(handler)
    logger.propagate = False

    return logger


logger = get_logger('TEST')
logger.info('Crawler starts ...')


class Crawler:

    def __init__(self, url, depth, crawl_batch_len=2):
        self.url = url
        self.depth = depth
        self.crawl_batch_len = crawl_batch_len
        self.aux_index = defaultdict(set)
        self.list_of_outdated_docs = set()

        self.filename_to_idx = dict()
        self.idx_to_filename = dict()

    @staticmethod
    def get_outdated_docs(old, new):
        old = set(old)
        new = set(new)
        return old - new

    @staticmethod
    def get_updated_docs(old, new):
        old = set(old)
        new = set(new)
        return new - old

    def upd_tmp_index(self, aux_index, list_of_removed_docs):
        logger.info('Aux index of length {} is added'.format(len(aux_index)))
        for term in aux_index:
            self.aux_index[term] = set.union(self.aux_index[term], aux_index[term])
        self.list_of_outdated_docs = set.union(self.list_of_outdated_docs, list_of_removed_docs)

    def send_to_workers(self):
        logger.info('Aux index of length {} is sent'.format(len(self.aux_index)))
        # send self.aux_index to all workers in json format
        # send self.list_of_outdated_docs to all workers
        requests.get(f'http://index:7000/update', params={'aux': 2})

    def upd_persistent_idx_mapping(self):
        json.dump(self.idx_to_filename, open('/mappings/idx_to_filename.json', 'w'))

    # TODO: scheduler of activations; remove list, aux index on new new(updated) docs, crawl only K new docs
    def run(self):
        self.downloader = Downloader(self.url, self.depth, cache_dir='/documents')
        inv_index = Inverted_index()

        current_docs = [name for name in os.listdir(self.downloader.cache_dir) \
                        if os.path.isfile(os.path.join(self.downloader.cache_dir, name))]
        recent_docs = []
        counter = 0
        doc_filename_to_obj = {}
        for c in self.downloader.crawl_generator():
            counter += 1
            recent_docs.append(c.filename)

            self.filename_to_idx[c.filename] = counter
            self.idx_to_filename[counter] = c.filename
            doc_filename_to_obj[c.filename] = c

            if counter == len(current_docs) + self.crawl_batch_len: break

        list_of_removed_docs = self.get_outdated_docs(current_docs, recent_docs)
        list_of_updated_docs = self.get_updated_docs(current_docs, recent_docs)
        logger.info('New docs: ' + str(list_of_updated_docs))

        collection = [(self.filename_to_idx[filename], doc_filename_to_obj[filename]) for filename in
                      list_of_updated_docs]
        aux_index = inv_index.make_index(collection)
        self.upd_persistent_idx_mapping()
        self.upd_tmp_index(aux_index, list_of_removed_docs)


c = Crawler('http://lyrics.com', 2)

schedule.every(6).seconds.do(c.run)
schedule.every(15).seconds.do(c.send_to_workers)
while 1:
    schedule.run_pending()
    time.sleep(1)