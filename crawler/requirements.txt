fastapi
uvicorn
colorlog==3.0.1
requests==2.23.0
nltk
beautifulsoup4
schedule
numpy