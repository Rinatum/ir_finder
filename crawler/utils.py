import requests
from urllib.parse import quote
import os
from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib.parse
from queue import Queue
import shutil


class Document:
    
    def __init__(self, url, cache_dir='cache'):
        self.url = url
        self.content = None
        filename = url
        filename = filename.replace('http://', '')
        filename = filename.replace('/', '_')
        self.filename = filename
        self.cache_dir = cache_dir
        os.makedirs(self.cache_dir, exist_ok=True)
        
    def get(self):
        if not self.load():
            if not self.download():
                raise FileNotFoundError(self.url)
            else:
                self.persist()
                pass
        else:
            pass
    
    def download(self):
        #TODO download self.url content, store it in self.content and return True in case of success
        try:
            if 'javascript' in self.url: return False
            r = requests.get(self.url, allow_redirects=True)
        except Exception:
            return False
        if r.status_code == 200:
            self.content = r.content
            return True
        return False
    
    def persist(self):
        with open(os.path.join(self.cache_dir, self.filename), 'xb') as f:
            f.write(self.content)        
            
    def load(self):
        
        #TODO load content from hard drive, store it in self.content and return True in case of success
        try:
            self.content = open(os.path.join(self.cache_dir, self.filename), 'rb').read()
            return True
        except OSError:
            return False
        return False
    
    
class HtmlDocument(Document):
    
    def __init__(self, *args):
        super().__init__(*args)
        self.get()
        self.parse()
    
    def parse(self):
        def tag_visible(element):
            if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
                return False
            if isinstance(element, Comment):
                return False
            return True
        #TODO exctact plain text, images and links from the document
        soup = BeautifulSoup(self.content, 'html.parser')
        texts = soup.findAll(text=True)
        visible_texts = filter(tag_visible, texts)  
        self.text = " ".join(t.strip() for t in visible_texts)
        
        self.anchors = []
        links = soup.findAll('a')
        for link in links:
            self.anchors.append((link.get('href'), urllib.parse.urljoin(self.url, link.get('href'))))

        self.images = []
        images = soup('img')
        for img in images:
            self.images.append(urllib.parse.urljoin(self.url, img.get('src')))
            
            
class Crawler:
    
    def __init__(self, url, depth = 1, cache_dir='cache'):
        self.q = Queue()
        self.seen = set()
        self.depth = depth
        self.url = url
        self.cache_dir = cache_dir
#         shutil.rmtree(self.cache_dir, ignore_errors=True)
        os.makedirs(self.cache_dir, exist_ok=True)
    
    def crawl_generator(self):
        return self.__crawl_generator__(self.url, self.depth)
    
    
    def __crawl_generator__(self, source, depth=1):
        if depth <= 0: 
            return
        if source not in self.seen:
            try:
                doc = HtmlDocument(source, self.cache_dir)
                self.seen.add(source)
                yield doc

                for tag, link in doc.anchors:
                    if source in link:
                        self.q.put((link, depth-1))

            except FileNotFoundError:
                print('cant load: ', source)

        if not self.q.empty():
            yield from self.__crawl_generator__(*self.q.get())
            




