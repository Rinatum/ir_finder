from abc import ABC, abstractmethod
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import WordPunctTokenizer
import re
from collections import defaultdict
nltk.download('stopwords')
nltk.download('wordnet')


class Base_index(ABC):
    
    def __init__(self):
        super().__init__()
        
    @abstractmethod
    def normalize(text: str) -> str:
        pass
    
    @abstractmethod
    def tokenize(text: str) -> str:
        pass
    
    @abstractmethod
    def lemmatize(text: str) -> str:
        pass
    
    @abstractmethod
    def remove_stop_word(text: str) -> str:
        pass
    
    @abstractmethod
    def make_index(collection: list) -> dict:
        """
        return: {token1: [doc_1, doc_2, ..., doc_n]}
        """
        pass
    
    
class Inverted_index(Base_index):
    
    @staticmethod
    def normalize(text):
        ret = [i for i in re.split('\W', text.lower()) if i.isalpha()]
        ret = ' '.join(ret)
        return ret
    
    @staticmethod
    def tokenize(text):
        tokenizer = WordPunctTokenizer()
        return tokenizer.tokenize(text)
    
    @staticmethod
    def lemmatize(tokens):
        lemmatizer = WordNetLemmatizer()
        return [lemmatizer.lemmatize(token) for token in tokens]
    
    @staticmethod
    def remove_stop_word(tokens):
        stopWords = set(stopwords.words('english'))
        return [i for i in tokens if i not in stopWords]
    
    @classmethod
    def preprocess(cls, text):
        ret = cls.normalize(text)
        ret = cls.tokenize(ret)
        ret = cls.lemmatize(ret)
        ret = cls.remove_stop_word(ret)
        return ret
    
    def make_index(self, collection):
        index = defaultdict(set)
        for doc_id, doc in collection:
            for word in self.preprocess(doc.text):
                documents = index[word]
                documents.add(doc_id)
        self.index = index
        return index
    
    def search(self, query):
        query = self.preprocess(query)
        docs_for_each_token = [self.index[i] for i in query]
        relevant_documents = set.intersection(*docs_for_each_token)
        return relevant_documents