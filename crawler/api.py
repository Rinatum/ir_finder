import colorlog
import logging
import random
import time
import requests

from fastapi import FastAPI, Request


import colorlog
import logging

LOGGING_LEVEL = 'INFO'
LOGGING_FORMAT = '[%(asctime)s] %(log_color)s%(message)s'


def get_logger(name=None):
    logger = colorlog.getLogger(name)
    logger.setLevel(logging.getLevelName(LOGGING_LEVEL))

    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter(LOGGING_FORMAT, datefmt='%Y-%m-%d %H:%M:%S'))

    logger.addHandler(handler)
    logger.propagate = False

    return logger


if __name__ == "__main__":
    logger = get_logger('TEST')
    logger.info('Crawler starts ...')
    current_time = 0
    while True:
        time.sleep(45)
        current_time += 2
        logger.info(f'Works already {current_time} seconds')
        doc_ids = requests.get(f'http://service:8000/search', params={'query': 'one second'})
        # doc_ids = requests.get(f'http://index:7000/health-check')
        print(doc_ids)

# if __name__ == "__main__":
#     client = pymongo.MongoClient('mongodb', 27017)
#     db = client["inverted_index"]
#     posting = db["word1"]
#     posting.remove({})
#     doc_list = [
#         {"doc_id": 1, "doc_path": "Apple st 652"},
#         {"doc_id": 2, "doc_path": "Apple st 652"},
#         {"doc_id": 3, "doc_path": "Apple st 652"},
#         {"doc_id": 4, "doc_path": "Apple st 652"},
#     ]
#     x = posting.insert_many(doc_list)
#     # print list of the _id values of the inserted documents:
#     print([doc["doc_id"] for doc in posting.find()])
#
#     posting = db["word2"]
#     posting.remove({})
#     doc_list = [
#         {"doc_id": 1, "doc_path": "Apple st 652"},
#         {"doc_id": 2, "doc_path": "Apple st 652"},
#         {"doc_id": 3, "doc_path": "Apple st 652"},
#         {"doc_id": 4, "doc_path": "Apple st 652"},
#     ]
#
#     x = posting.insert_many(doc_list)
#     # print list of the _id values of the inserted documents:
#     print([doc["doc_id"] for doc in posting.find()])
# HUUUY RABOTAET


